﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    //sprites for the crates
    //these values are assigned in the unity editor (Serializing the variable allows me to view it in the unity inspector)
    [SerializeField] Sprite crateNormalSprite;
    [SerializeField] Sprite crateExplosiveSprite;

    //crate variables
    Crate[,] crates = new Crate[4,5]; //stores where all the crates are placed

    //variables for upcomming crates
    List<Crate> upcommingCrates = new List<Crate>();
    List<float> crateDropTimes = new List<float>();

    //score
    [SerializeField] Text scoreText;
    [SerializeField] Text highscoreText;
    int score = 0;
    int highscore = 0;

    [SerializeField] UIManager UImngr;
    [SerializeField] GameObject player;
    public bool isPaused = true;
    bool LostGame = false;
    
    //will spawn a crate somewhere in the level
    void SpawnCrate(CrateType type, int pos, float timeUntilFall){
        //Initializing a new crate
        Crate crate = new Crate(new GameObject("Crate"), type);

        //add a Sprite to the gameobject and set the sprite to the corresponding sprite
        crate.gameObject.AddComponent<SpriteRenderer>().sprite = type == CrateType.normal ? crateNormalSprite : crateExplosiveSprite;
        crate.gameObject.transform.position = new Vector3(pos,5,0); //set the crates position

        //add a colider to the gameobject
        crate.gameObject.AddComponent<BoxCollider2D>(); //the default size is one

        //set the layer of the object (needed for the raycast that checks if the player is grounded)

        
        upcommingCrates.Add(crate); //make this crate an upcomming crate
        crateDropTimes.Add(timeUntilFall); //set the time until drop for the crate
    }

    //make an instantiated crate fall
    void FallCrate(Crate crate){
        uint originalRow = (uint)crate.gameObject.transform.position.y; //this is needed for removing the previous position
        uint column      = (uint)crate.gameObject.transform.position.x; //the column of which the crate is in
        bool foundCollision = false; //if it hasnt found a collision it will put the crate at row 0

        //loop through every position under this crate until it has found a collision
        for(int i = (int)crate.gameObject.transform.position.y - 2; i >= 0; i--){
            //check if it is colliding
            if(crates[column, i] != null){
                if (originalRow < 5) { crates[column, originalRow] = null;} //remove the old position of the crate

                crates[column, i + 1] = crate; //set the new position of the crate
                crate.gameObject.transform.position = new Vector3(column, i + 1, 0); //make sure the crate appears at the right position

                foundCollision = true;
                break;
            }
        }

        //if it hasnt found a collision it will put the row to 0
        if(!foundCollision) {
            if (originalRow < 5) { crates[column, originalRow] = null; } //remove the old position
            
            crates[column, 0] = crate; //set the new position
            crate.gameObject.transform.position = new Vector3(column, 0, 0); //make sure the crate appears at the right position
        }
    }

    public void BreakCrate(Vector2Int position){
        //check if the crate that is being broken is explosive
        if(crates[position.x, position.y] != null){
            if(crates[position.x, position.y].type == CrateType.explosive){
                //an explosive crate will destroy boxes in a 3x3 area

                //a rectangle in which to destroy crates (bottom-left, top-right)
                Vector2Int explosionBL = new Vector2Int((position.x - 1) < 0 ? 0 : position.x - 1, (position.y - 1) < 0 ? 0 : position.y - 1);
                Vector2Int explosionTR = new Vector2Int((position.x + 1) > 3 ? 3 : position.x + 1, (position.y + 1) > 4 ? 4 : position.y + 1);

                //loop through every position in the explosion
                for (    int x = explosionBL.x; x <= explosionTR.x; x++){
                    for (int y = explosionBL.y; y <= explosionTR.y; y++){
                        //remove crates
                        if(crates[x,y] != null){
                            Destroy(crates[x,y].gameObject);
                            crates[x,y] = null;
                        }
                    }
                    
                    //make the crates above the explosion fall
                    for(int y = explosionTR.y + 1; y < 5; y++){
                        if(crates[x,y] != null){FallCrate(crates[x,y]);}
                    }
                }
            } else{
                //Loop through all crates above the one being broken
                Destroy(crates[position.x,position.y].gameObject); //destroy the gameobject of the crate
                crates[position.x,position.y] = null; //remove the crate
                
                for(int i = position.y + 1; i < 5; i++){
                    if(crates[position.x, i] == null){ break; } //stop looping if there are no more crates above
                    FallCrate(crates[position.x,i]); //make the crate fall
                }
            }
        }
    }

    void LoseGame(){
        //update Highscore
        highscore = score > highscore ? score : highscore;
        highscoreText.text = highscore.ToString();

        //reset the game
        ResetGame();
        isPaused = true;
        UImngr.ShowMenu();
    }

    void ResetGame(){
        //reset "crates" variable
        for(int x = 0; x < 4; x++){
            for (int y = 0; y < 5; y++){
                if(crates[x,y] != null){
                    Destroy(crates[x,y].gameObject); //remove each gameobject
                    crates[x,y] = null; //remove the crate from crates
                }
            }
        }
        //reset upcommingCrates
        foreach(Crate c in upcommingCrates){ Destroy(c.gameObject); }
        upcommingCrates = new List<Crate>();
        crateDropTimes = new List<float>();

        player.transform.position = new Vector3(2,2,0); //reset the player's position
        score = 0; //reset score
        scoreText.text = "0";

        Debug.Log("Reset game");
    }

    // Update is called once per frame
    void Update()
    {
        if(!isPaused){
            //loop through every upcomming crate
            #region upcommingCrates
            List<int> droppedCratesIndex = new List<int>(); //a list for all the crates that need to be removed from upcommingCrates

            for(int i = 0; i < crateDropTimes.Count; i++){
                crateDropTimes[i] -= Time.deltaTime; //reduce the time till drop by time since the last Update() ran
                if(crateDropTimes[i] <= 0){ //Check if it should fall this Update()
                    int column = (int)upcommingCrates[i].gameObject.transform.position.x; //the column the crate is supposed to be in
                    
                    if(crates[column, 4] != null || Mathf.FloorToInt(player.transform.position.x) == column) { LoseGame(); break;} //check if you should lose the game

                    FallCrate(upcommingCrates[i]); //make the crate fall
                    droppedCratesIndex.Add(i); //remove the crate later from upcommingCrates

                    score++;
                    scoreText.text = score.ToString();
                }
            }

            //remove the upcommingCrates that already dropped
            foreach(int i in droppedCratesIndex){ 
                upcommingCrates.RemoveAt(i); 
                crateDropTimes.RemoveAt(i);
            }
            #endregion

            //spawn new crates, but only one at a time
            if (upcommingCrates.Count == 0){
                //SpawnCrate(5% to be explosive, random colum 1-4, 1s till it drops)
                SpawnCrate(Random.value > .1f ? CrateType.normal : CrateType.explosive, Mathf.FloorToInt(Random.Range(0,4)), 1f);
            }
        }
        if(Input.GetKeyUp(KeyCode.Escape)){ isPaused = !isPaused; UImngr.ToggleMenu(); } //pausing
    }

    public void Continue(){ isPaused = false; UImngr.HideMenu();}
    public void Exit() { Application.Quit(); }

    //when the game loses focus pause the game
    void OnApplicationFocus(bool hasFocus){ if(!hasFocus){ isPaused = true; UImngr.ShowMenu(); }}

    //Debug
    //This draws Gizmos whenever the gameobject is selected in the editor (shows only in the scene view, not in game view)
    void OnDrawGizmosSelected(){
        //set the offset
        Vector3 offset = new Vector3(.5f,.5f,0);
        
        //loop through the x & y of the playing field
        for(int x = 0; x < 4; x++){
            for (int y = 0; y < 5; y++)
            {
                Gizmos.color = Color.green; //set the default drawing color to green
                if(crates[x,y] == null) { Gizmos.color = Color.red; } //if there is no crate it should change the color to red

                //draw the gizmo
                Gizmos.DrawCube(new Vector3(x,y,0) + offset, Vector3.one * .1f); 
            }
        }
    }
}

public enum CrateType{
    normal,
    explosive
}