﻿using UnityEngine;

public class Crate
{
    public GameObject gameObject {get; private set;}
    public CrateType  type  {get; private set;}

    public Crate(GameObject _crate, CrateType _type){
        gameObject = _crate;
        type = _type;
    }
}
