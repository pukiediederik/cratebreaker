﻿using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject menuCanvas;
    [SerializeField] GameObject startBtn;
    [SerializeField] GameObject exitBtn;

    public void ShowMenu(){ menuCanvas.SetActive(true); }

    public void HideMenu(){ menuCanvas.SetActive(false); }

    public void ToggleMenu(){  menuCanvas.SetActive(!menuCanvas.activeSelf);}
}
