﻿using UnityEngine;

//this script requires there to be a rigidbody2d component on this gameobject
[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    BoxCollider2D col;
    [SerializeField] GameManager gm;

    //movement
    const float moveSpeed = 2.5f;
    [SerializeField]bool lookDir = false; //false = left, true = right

    const float jumpPower = 7;
    const float gravity = .2f;
    float VerticalVelocity = 0;

    //breaking blocks
    const float breakSpeed = .75f; //The delay between breaking blocks
    float breakDelay; //time since last broken block


    //runs before the first frame is rendered
    void Start(){

        rb = this.GetComponent<Rigidbody2D>(); //gets the rigidbody on this gameobject
        col = this.GetComponent<BoxCollider2D>();
    }

    //runs every physics update, this is needed because RigidBody2d.MovePosition updates every fixedUpdate
    void FixedUpdate(){

        if (!gm.isPaused){
            if(Input.GetAxis("Horizontal") != 0) {lookDir = Input.GetAxis("Horizontal") > 0 ? true: false;} //set the lookdir
            
            #region Breaking Crates
                if(Input.GetAxis("Break") > 0 && breakDelay <= 0){ //check if the player can and wants to break
                    Vector2Int playerpos = new Vector2Int(Mathf.FloorToInt(this.transform.position.x), Mathf.FloorToInt(this.transform.position.y)); //player position floored to int

                    Vector2Int breakOffset = Input.GetAxis("Vertical") < 0 ? Vector2Int.down : new Vector2Int(lookDir ? 1 : -1,0); //determined by where the player is looking
                    Vector2Int breakPos = playerpos + breakOffset; //where the player will actually try to break

                    if((breakPos.x >= 0 && breakPos.x < 4) && (breakPos.y >= 0 && breakPos.y < 5)){ //check if this breakpos is a valid position to break
                        gm.BreakCrate(breakPos); //break the block if its a valid position
                        breakDelay = breakSpeed; //reset the break timer
                    }
                }

                breakDelay -= Time.fixedDeltaTime;
            #endregion

            #region movement
            float colWidth  = col.size.x / 2;
            float colHeight = col.size.y / 2;

            //check if player is grounded
            bool isGrounded = Physics2D.Raycast((Vector2)this.transform.position + new Vector2(colWidth , -colHeight - 0.01f), Vector2.down, .01f) ||
                            Physics2D.Raycast((Vector2)this.transform.position + new Vector2(-colWidth, -colHeight - 0.01f), Vector2.down, .01f);
            VerticalVelocity -= gravity; //apply gravity

            //check if the player can jump and if it wants to jump.
            if(isGrounded && Input.GetAxis("Jump") > 0){ VerticalVelocity = jumpPower; }
            else if(isGrounded) {VerticalVelocity = 0;}

            //figure out the new position of the player (in local space)
            Vector2 newPos = new Vector2(Input.GetAxis("Horizontal") * moveSpeed, VerticalVelocity) * Time.fixedDeltaTime; 
            rb.MovePosition(newPos + (Vector2)this.transform.position);
            #endregion
        }
    }
}